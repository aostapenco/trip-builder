<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $table = 'trips';
    protected $hidden = ['created_at', 'updated_at'];

    public function flights()
    {
        return $this->hasMany('App\Flight');
    }

    public function buildDeepArray()
    {
        $trip = $this->toArray();

        $flights = array();
        foreach ($this->flights as $flight) {
            $flightArr = $flight::with(array('fromAirport', 'toAirport'))->find($flight->id)->toArray();
            $cityFromArr = $flight->fromAirport->city->toArray();
            $cityToArr = $flight->toAirport->city->toArray();
            $countryFromArr = $flight->fromAirport->city->country->toArray();
            $countryToArr = $flight->toAirport->city->country->toArray();
            $cityFromArr["country"] = $countryFromArr;
            $cityToArr["country"] = $countryToArr;
            $flightArr['from_airport']["city"] = $cityFromArr;
            $flightArr['to_airport']["city"] = $cityToArr;
            $flights[] = $flightArr;
        }

        $trip['flights'] = $flights;

        return $trip;
    }
}
