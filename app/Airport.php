<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $table = 'airports';
    protected $hidden = ['city_id'];
    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function buildDeepArray()
    {
        $airport = $this->toArray();
        $cityArr = $this->city->toArray();
        $countryArr = $this->city->country->toArray();
        $cityArr["country"] = $countryArr;
        $airport["city"] = $cityArr;

        return $airport;
    }
}
