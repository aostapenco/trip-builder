<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flights';
    protected $hidden = ['trip_id', 'from_air_id', 'to_air_id', 'created_at', 'updated_at'];
    protected $fillable = ['trip_id', 'from_air_id', 'to_air_id'];

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }

    public function fromAirport()
    {
        return $this->belongsTo('App\Airport', 'from_air_id');
    }

    public function toAirport()
    {
        return $this->belongsTo('App\Airport', 'to_air_id');
    }
}
