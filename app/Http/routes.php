<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// #### TRIP ROUTES #######################################################

Route::get('api/trip/{tripId}', 'TripController@get');
Route::put('api/trip/{tripId}', 'TripController@update');

// #### AIRPORT ROUTES #####################################################

Route::get('api/airports', 'AirportController@listAll');

// #### FLIGHT ROUTES ######################################################

Route::post('api/flight/{tripId}', 'FlightController@create');
Route::delete('api/flight/{flightId}', 'FlightController@destroy');