<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Trip;
use Illuminate\Http\Request;


class TripController extends Controller
{
    public function get($tripId) {
        $trip = Trip::findOrFail($tripId);

        return $trip->buildDeepArray();
    }

    public function update(Request $request, $tripId) {
        if ($request->has('name')) {
            $trip = Trip::findOrFail($tripId);
            $trip->name = $request->input('name');
            $trip->save();

            return $trip->buildDeepArray();
        }
        else {
            abort(400, "Missing trip name parameter");
        }
    }
}