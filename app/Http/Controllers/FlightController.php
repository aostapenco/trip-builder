<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Flight;
use Illuminate\Http\Request;


class FlightController extends Controller
{
    /**
     * Create new flight.
     * Json fromAirportId and toAirportId are requested
     *
     * @param Request $request
     * @param $tripId Id of the associated trip
     * @return static Newly created flight
     */
    public function create(Request $request, $tripId) {
        if ($request->has('fromAirportId') && $request->has('toAirportId')) {
            $newFlight = Flight::create(array(
                'trip_id' => $tripId,
                'from_air_id' => $request->input('fromAirportId'),
                'to_air_id' => $request->input('toAirportId')
            ));

            return $newFlight;
        }
        else {
            abort(400, "Missing fromAirportId and/or toAirportId parameters");
        }
    }

    public function destroy($flightId) {
        $flight = Flight::findOrFail($flightId);
        $isDeleted = $flight->delete();

        if ($isDeleted) {
            return response("Flight was successfuly deleted");
        }
        else {
            abort(500, "Error while deleting the flight");
        }
    }
}