<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Airport;
use Illuminate\Http\Request;


class AirportController extends Controller
{
    public function listAll() {
        $airports = Airport::orderBy('name', 'asc')->get();

        return $airports->map(function ($airport) {
            return $airport->buildDeepArray();
        });
    }
}