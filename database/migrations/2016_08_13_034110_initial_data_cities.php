<?php
use App\City;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDataCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = array(
            array('name' => 'Montreal', 'country_id' => 1),
            array('name' => 'New York', 'country_id' => 2),
            array('name' => 'Berlin', 'country_id' => 3),
            array('name' => 'Munich', 'country_id' => 3),
            array('name' => 'London', 'country_id' => 4)
        );

        City::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
        DB::table('cities')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
