<?php
use App\Airport;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDataAirports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = array();

        // Montreal
        $data[] = array('name' => 'Montréal-Pierre Elliott Trudeau International Airport', 'code' => 'YUL', 'city_id' => 1);
        $data[] = array('name' => 'Montreal International (Mirabel)', 'code' => 'YMX', 'city_id' => 1);

        // New York
        $data[] = array('name' => 'John F Kennedy International', 'code' => 'JFK', 'city_id' => 2);
        $data[] = array('name' => 'La Guardia', 'code' => 'LGA', 'city_id' => 2);

        // Berlin
        $data[] = array('name' => 'Berlin-Schönefeld International', 'code' => 'SXF', 'city_id' => 3);
        $data[] = array('name' => 'Berlin-Tempelhof International', 'code' => 'THF', 'city_id' => 3);

        // Munich
        $data[] = array('name' => 'Franz Josef Strauss International Airport', 'code' => 'MUC', 'city_id' => 4);

        // London
        $data[] = array('name' => 'London Luton', 'code' => 'LTN', 'city_id' => 5);
        $data[] = array('name' => 'London Biggin Hill', 'code' => 'BQH', 'city_id' => 5);

        Airport::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
        DB::table('airports')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }
}
