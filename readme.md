# Trip Builder API

Web application is based on Laravel Framework.

- Copy .env.local file to .env and change/use DB settings.

- Install [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)

- Check [Laravel Framework requirements](https://www.laravel.com/docs/5.2).
  N.B. Directories within the `storage` and the `bootstrap/cache` directories should be writable by your web server.
  You can change group to `www-data` after git clone.
  ```
  sudo chown :www-data -R storage bootstrap/cache
  ```

- To install dependencies run in the project root directory:
```
composer install
```

- To add initial data to the database run in the project root directory:
```
php artisan migrate
```
- Nginx config example (PHP 7):
```
server {
    listen 80;
    listen [::]:80;
    
    server_name my.trip.ca;
    
    root /var/www/tripBuilder/public;
    index index.php;
    
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    
    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```

- API Routes
```
GET    api/trip/{tripId}     - get a trip by id
PUT    api/trip/{tripId}     - update a trip name. JSON 'name' option is required: {"name": "New name"}
GET    api/airports          - get a list of airports ordered by name
POST   api/flight/{tripId}   - create a new flight. JSON required {"fromAirportId": 1, "toAirportId": 2}
DELETE api/flight/{flightId} - delete a flight
```

